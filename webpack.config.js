var path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'www', 'js'),
    filename: 'bundle.js'
  },
  // externals: {
  //   'dcp-client': {
  //     commonjs: 'dcp-client'
  //   }
  // },
  devServer: {
    contentBase: path.join(__dirname, 'www'),
    publicPath: '/js/',
    watchContentBase: true,
    compress: true,
    port: 8080
  }
};